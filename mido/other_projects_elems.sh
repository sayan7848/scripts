#!/bin/bash

# Bring some elements from other projects 

rm -rf hardware/qcom/bt
rm -rf packages/apps/Bluetooth
rm -rf packages/apps/Stk
rm -rf packages/apps/DeskClock
rm -rf packages/apps/FMRadio
rm -rf packages/apps/Recorder
rm -rf hardware/qcom/fm
rm -rf external/ant-wireless/antradio-library
rm -rf device/qcom/sepolicy
rm -rf packages/apps/Phonograph
rm -rf packages/inputmethods/LatinIME

git clone https://github.com/CypherOS/device_qcom_sepolicy.git -b oreo-mr1-release device/qcom/sepolicy
git clone https://github.com/LineageOS/android_hardware_qcom_bt.git -b lineage-15.1-caf hardware/qcom/bt
git clone https://github.com/LineageOS/android_packages_apps_Bluetooth.git -b lineage-15.1 packages/apps/Bluetooth
git clone https://github.com/LineageOS/android_packages_apps_Stk.git -b lineage-15.1 packages/apps/Stk

git clone https://github.com/LineageOS/android_hardware_qcom_fm.git -b lineage-15.1 hardware/qcom/fm
git clone https://github.com/LineageOS/android_packages_apps_FMRadio.git -b lineage-15.1 packages/apps/FMRadio
git clone https://github.com/LineageOS/android_packages_apps_Recorder.git -b lineage-15.1 packages/apps/Recorder
git clone https://github.com/LineageOS/android_external_ant-wireless_antradio-library.git -b lineage-15.1 external/ant-wireless/antradio-library
git clone https://github.com/Cardinal-AOSP/packages_apps_DeskClock.git -b oreo-mr1 packages/apps/DeskClock
git clone https://github.com/omnirom/android_packages_apps_Phonograph.git -b android-8.1 packages/apps/Phonograph
git clone https://github.com/LineageOS/android_packages_inputmethods_LatinIME.git -b lineage-15.1 packages/inputmethods/LatinIME
