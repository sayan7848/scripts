#!/bin/bash

# Remove pre-existing hals and replace with compatible ones

rm -rf hardware/qcom/audio-caf/msm8996
rm -rf hardware/qcom/display-caf/msm8996
rm -rf hardware/qcom/media-caf/msm8996

git clone https://github.com/LineageOS/android_hardware_qcom_audio.git -b lineage-15.1-caf-8996 hardware/qcom/audio-caf/msm8996
git clone https://github.com/LineageOS/android_hardware_qcom_display.git -b lineage-15.1-caf-8996 hardware/qcom/display-caf/msm8996
git clone https://github.com/LineageOS/android_hardware_qcom_media.git -b lineage-15.1-caf-8996 hardware/qcom/media-caf/msm8996
