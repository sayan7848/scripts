#!/bin/bash

# Clone device, vendor and kernel trees

rm -rf device/xiaomi/mido
rm -rf vendor/xiaomi
rm -rf kernel/xiaomi/msm8953

git clone https://github.com/sayan7848/android_device_xiaomi_mido.git -b oreo-mr1 device/xiaomi/mido
git clone https://github.com/sayan7848/proprietary_vendor_xiaomi.git -b oreo-mr1 vendor/xiaomi
git clone https://github.com/LordArcadius/electrablue_mido.git -b oreo kernel/xiaomi/msm8953
