#!/bin/bash

# Clone device, vendor and kernel trees

rm -rf device/xiaomi/mido
rm -rf vendor/xiaomi
rm -rf kernel/xiaomi/msm8953
rm -rf device/xiaomi/msm8953-common

git clone https://github.com/sayan7848/android_device_xiaomi_mido.git -b myadditions device/xiaomi/mido
git clone https://github.com/sayan7848/android_device_xiaomi_msm8953-common.git -b myadditions device/xiaomi/msm8953-common
git clone https://github.com/sayan7848/proprietary_vendor_xiaomi-1.git -b myadditions vendor/xiaomi
git clone https://github.com/sayan7848/android_kernel_xiaomi_msm8953.git kernel/xiaomi/msm8953
