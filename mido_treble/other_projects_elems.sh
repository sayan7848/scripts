#!/bin/bash

# Bring some elements from other projects 

rm -rf packages/apps/DeskClock
rm -rf packages/apps/FMRadio
rm -rf packages/apps/Recorder
rm -rf hardware/qcom/fm
rm -rf external/ant-wireless/antradio-library
rm -rf packages/inputmethods/LatinIME
rm -rf packages/apps/Stk
rm -rf packages/apps/Eleven
rm -rf packages/apps/Snap

git clone https://github.com/DirtyUnicorns/android_hardware_qcom_fm.git hardware/qcom/fm
git clone https://github.com/DirtyUnicorns/android_packages_apps_FMRadio.git packages/apps/FMRadio
git clone https://github.com/DirtyUnicorns/android_packages_apps_DeskClock.git packages/apps/DeskClock
git clone https://github.com/LineageOS/android_packages_apps_Recorder.git -b lineage-15.1 packages/apps/Recorder
git clone https://github.com/LineageOS/android_external_ant-wireless_antradio-library.git -b lineage-15.1 external/ant-wireless/antradio-library
git clone https://github.com/DirtyUnicorns/android_packages_inputmethods_LatinIME.git packages/inputmethods/LatinIME
git clone https://github.com/DirtyUnicorns/android_packages_apps_Stk.git packages/apps/Stk
git clone https://github.com/LineageOS/android_packages_apps_Eleven.git packages/apps/Eleven
git clone https://github.com/AospExtended/platform_packages_apps_Snap.git packages/apps/Snap

